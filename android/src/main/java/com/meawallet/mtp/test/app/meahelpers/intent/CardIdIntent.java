package com.meawallet.mtp.test.app.meahelpers.intent;

import android.content.Context;

import com.meawallet.mtp.test.app.meahelpers.constants.IntentConstants;

public class CardIdIntent extends AppIntent {

  public CardIdIntent(Context context, Class<?> activityClass, String action, String cardId) {
    super(context, activityClass, action);

    putExtra(IntentConstants.INTENT_DATA_CARD_ID_KEY, cardId);
  }
}
