package com.meawallet.mtp.test.app.meahelpers.receivers;

import android.content.Context;

import com.meawallet.mtp.MeaContactlessTransactionData;
import com.meawallet.mtp.MeaError;
import com.meawallet.mtp.MeaTransactionReceiver;
import com.meawallet.mtp.test.app.FlutterMeaWalletQaPlugin;
import com.meawallet.mtp.test.app.meahelpers.TransactionEventsHandler;
import com.meawallet.mtp.test.app.utils.Logger;;

/**
 * Contactless transaction events receiver.
 */
public class TransactionReceiver extends MeaTransactionReceiver {

  private static final String TAG = TransactionReceiver.class.getSimpleName();

  private final TransactionEventsHandler mTransactionEventsHandler = new TransactionEventsHandler();

  @Override
  public void handleOnTransactionSubmittedIntent(Context context, String cardId, MeaContactlessTransactionData data) {
    Logger.d(TAG, "handleOnTransactionSubmittedIntent(cardId = %s, data = %s)", cardId, data);
    FlutterMeaWalletQaPlugin.sendMessageToFlutter("TrxListenerRes", data.getMerchantAndLocation(), FlutterMeaWalletQaPlugin.REMOTE_TRANSACTION_SUCCESS, data.getAmount().intValue());
  }

  @Override
  public void handleOnTransactionFailureIntent(Context context, String cardId, MeaError error, MeaContactlessTransactionData data) {
    Logger.e(TAG, "handleOnTransactionFailureIntent(cardId = %s)", error, cardId);
    FlutterMeaWalletQaPlugin.sendMessageToFlutter("TrxListenerRes", "",FlutterMeaWalletQaPlugin.REMOTE_TRANSACTION_FAILURE, null);

    //mTransactionEventsHandler.handleTransactionFailureEvent(context, cardId, error, data, false);
  }

  @Override
  public void handleOnAuthenticationRequiredIntent(Context context, String cardId, MeaContactlessTransactionData data) {
    Logger.v(TAG, "handleOnAuthenticationRequiredIntent(cardId = %s, data = %s)", cardId, data);

    //mTransactionEventsHandler.handleAuthenticationRequiredEvent(context, cardId, data, false);
  }

  @Override
  public void handleOnTransactionStartedIntent(Context context, String cardId) {
    Logger.v(TAG, "handleOnTransactionStartedIntent(cardId = %s)", cardId);

    //mTransactionEventsHandler.handleTransactionStartedEvent(context, cardId, false);
  }
}
