package com.meawallet.mtp.test.app.meahelpers.listeners;

import androidx.annotation.NonNull;

import com.meawallet.mtp.MeaCard;
import com.meawallet.mtp.MeaCardState;
import com.meawallet.mtp.MeaDigitizedCardStateChangeListener;
import com.meawallet.mtp.test.app.FlutterMeaWalletQaPlugin;
import com.meawallet.mtp.test.app.utils.Logger;

public class DigitizedCardStateChangeListenerImpl implements MeaDigitizedCardStateChangeListener {

  private static final String TAG = DigitizedCardStateChangeListenerImpl.class.getSimpleName();

  @Override
  public void onStateChanged(MeaCard meaCard, @NonNull MeaCardState meaCardState) {
    Logger.d(TAG, "onStateChanged(cardId = %s, meaCardState = %s)", meaCard.getId(), meaCardState);
    if (meaCardState.equals(MeaCardState.ACTIVE)) {
      FlutterMeaWalletQaPlugin.sendMessageToFlutter("cardState", "", FlutterMeaWalletQaPlugin.CARD_ACTIVATION_COMPLETED, null);
    }
  }
}
