import Flutter
import UIKit

public class SwiftFlutterMeaWalletQaPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "flutter_mea_wallet_qa", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterMeaWalletQaPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    result("iOS " + UIDevice.current.systemVersion)
  }
}
