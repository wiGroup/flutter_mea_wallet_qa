#import "FlutterMeaWalletQaPlugin.h"
#if __has_include(<flutter_mea_wallet_qa/flutter_mea_wallet_qa-Swift.h>)
#import <flutter_mea_wallet_qa/flutter_mea_wallet_qa-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_mea_wallet_qa-Swift.h"
#endif

@implementation FlutterMeaWalletQaPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterMeaWalletQaPlugin registerWithRegistrar:registrar];
}
@end
